#######################
Enhanced CoreAPI client
#######################

Wrapper around ``coreapi.Client`` for more convenient usage.

Installation
============

.. code-block:: bash

   pip install enhanced-coreapi-client

Usage
=====

Initialize client:

.. code-block:: python

    from enhanced_coreapi_client import Client
    client = Client('https://example.com/api/schema/')


Access API endpoints in accordance with the schema, e.g.:

.. code-block:: python

   users = client.api.users.list()
   project = client.api.users.projects.read(id=7)
   new_project = client.api.users.projects.create(name='xxx', user_id=3)
