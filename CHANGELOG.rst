1.3.1 (unreleased)
------------------

- Nothing changed yet.


1.3.0 (2021-09-21)
------------------

- Decouple with Django.


1.2.0 (2021-03-19)
------------------

- A more general and flexible way of configuring the client.


1.1.0 (2017-08-20)
------------------

- Loosening Django version.


1.0.1 (2017-08-11)
------------------

- Performance improvement.


1.0.0 (2017-08-11)
------------------

- Releasing v1.0.0 to PyPi.


0.2.1 (2017-08-11)
------------------

- README.


0.2.0 (2017-08-11)
------------------

- Syntactic sugar.


0.1.0 (2017-08-10)
------------------

- Django CoreAPI client v0.1.0 released.
